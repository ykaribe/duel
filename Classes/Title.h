/*
 * Title.h
 *
 *  Created on: 2015/04/14
 *      Author: Toida
 */

#ifndef TITLE_H_
#define TITLE_H_

#include "cocos2d.h"

class Title : public cocos2d::CCLayer
{
private:
	/* 関数プロトタイプ */
	void GameMainStart();
	void CharacterSelectStart();
	void OptionStart();

public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::CCScene* scene();

	// implement the "static node()" method manually
	CREATE_FUNC(Title);

	/* cocos2dxで用意しているタッチ関連関数 */
//	virtual bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
//	virtual void ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
//	virtual void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);

	/* バックキー、ホームキー関数 */
//	virtual void keyBackClicked();
//	virtual void keyMenuClicked();
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);


	// a selector callback
	void menuCloseCallback(CCObject* pSender);
};


#endif /* TITLE_H_ */
